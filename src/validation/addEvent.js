import { isEmpty  } from 'ramda';

const validateAddEvent = (data) => {
  let errors = {};

  data.name = data.name && !isEmpty(data.name) ? data.name : "";
  data.description = data.description  && !isEmpty(data.description) ? data.description : "";
  data.calendar = data.calendar && !isEmpty(data.calendar) ? data.calendar : "";
  data.category = data.category  && !isEmpty(data.category) ? data.category : "";

  if (data.name.length < 2 || data.name.length > 30) {
    errors.name = 'Name must be between 2 and 30 characters';
  }
  
  if (data.name === "") {
    errors.name = "Name field is required";
  }

  if (data.description.length < 2 || data.description.length > 255) {
    errors.description = 'Description must be between 2 and 255 characters';
  }

  if (data.description === "") {
    errors.description = "Description field is required";
  }

  if (data.calendar === "") {
    errors.calendar = "Calendar field is required";
  }

  if (data.category === "") {
    errors.category = "Category field is required";
  }

  return { errors };
};

export default validateAddEvent;
