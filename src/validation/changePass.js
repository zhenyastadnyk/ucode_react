import { isEmpty  } from 'ramda';
import { ifAnyDigits, ifAnyLetters }from './general';

const validateChangePass = (data) => {
  var errors = {};

  data.password = !isEmpty(data.password) ? data.password : '';
  data.password_confirmation = !isEmpty(data.password_confirmation) ? data.password_confirmation : '';

  if (data.password === '') {
    errors.password = 'Password field is required';
  }

  if (!ifAnyDigits(data.password)) {
    errors.password = 'Password must contain at least one digit';
  }

  if (!ifAnyLetters(data.password)) {
    errors.password = 'Password must contain at least one letter';
  }

  if (data.password.length < 6 || data.password.length > 30) {
    errors.password = 'Password must be at least 6 characters';
  }

  if (data.password_confirmation === '') {
    errors.password_confirmation = 'Confirm password field is required';
  }

  if (data.password !== data.password_confirmation) {
    errors.password_confirmation = 'Passwords must match';
  }

  return { errors };
};

export default validateChangePass;
