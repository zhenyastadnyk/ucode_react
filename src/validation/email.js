import { isEmpty  } from 'ramda';
import { isEmail }from './general';

const validateEmail = (email) => {
  let errors = {};

  email = email && !isEmpty(email) ? email : "";

  if (!isEmail(email)) {
    errors.email = "Email is not valid";
  }

  if (email === "") {
    errors.email = "Email field is required";
  }

  return { errors };
};

export default validateEmail;