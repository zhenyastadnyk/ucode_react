import { isEmpty  } from 'ramda';
import { ifAnyDigits, ifAnyLetters, isEmail }from './general';

const validateRegister = (data) => {
  var errors = {};

  data.name = !isEmpty(data.name) ? data.name : '';
  data.email = !isEmpty(data.email) ? data.email : '';
  data.password = !isEmpty(data.password) ? data.password : '';
  data.password_confirmation = !isEmpty(data.password_confirmation) ? data.password_confirmation : '';

  if (data.name.length < 2 || data.name.length > 30) {
    errors.name = 'Name must be between 2 and 30 characters';
  }

  if (data.name === '') {
    errors.name = 'Name field is required';
  }

  if (data.email === '') {
    errors.email = 'Email field is required';
  }

  if (!isEmail(data.email)) {
    errors.email = 'Email is not valid';
  }

  if (data.password === '') {
    errors.password = 'Password field is required';
  }

  if (!ifAnyDigits(data.password)) {
    errors.password = 'Password must contain at least one digit';
  }

  if (!ifAnyLetters(data.password)) {
    errors.password = 'Password must contain at least one letter';
  }

  if (data.password.length < 6 || data.password.length > 30) {
    errors.password = 'Password must be at least 6 characters';
  }

  if (data.password_confirmation === '') {
    errors.password_confirmation = 'Confirm password field is required';
  }

  if (data.password !== data.password_confirmation) {
    errors.password_confirmation = 'Passwords must match';
  }

  return { errors };
};

export default validateRegister;
