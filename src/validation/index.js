import validateRegister from './register';
import validateLogin from './login';
import validateEmail from './email';
import validateChangePass from './changePass';
import validateAddEvent from './addEvent';
import validateEditEvent from './editEvent';
import validateAddCalendar from './addCalendar';
import validateEditCalendar from './editCalendar';

export {
  validateRegister,
  validateLogin,
  validateEmail,
  validateChangePass,
  validateAddEvent,
  validateEditEvent,
  validateAddCalendar,
  validateEditCalendar
};
