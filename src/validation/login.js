import { isEmpty  } from 'ramda';
import { isEmail }from './general';

const validateLogin = (data) => {
  let errors = {};

  data.email = !isEmpty(data.email) ? data.email : "";
  data.password = !isEmpty(data.password) ? data.password : "";

  if (!isEmail(data.email)) {
    errors.email = "Email is not valid";
  }

  if (data.email === "") {
    errors.email = "Email field is required";
  }

  if (data.password === "") {
    errors.password = "Password field is required";
  }

  return { errors };
};

export default validateLogin;
