const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
const isEmail = str => {
  return emailPattern.test(str);
};

const ifAnyDigitsPattern = /[0-9]+/;
const ifAnyDigits = str => {
  return ifAnyDigitsPattern.test(str);
};

const ifAnyLettersPattern = /[a-zA-Z]+/;
const ifAnyLetters = str => {
  return ifAnyLettersPattern.test(str);
};

export { ifAnyDigits, ifAnyLetters, isEmail };
