import React, { useEffect, useState } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import Modal from 'react-modal';
import TextFieldGroup from '../common/TextFieldGroup';
import { updateCalendar, getCalendars} from '../../actions/calendarActions';
import { getEvents } from '../../actions/eventActions';
import RadioSelect from '../common/RadioSelect';
import CloseIcon from '@material-ui/icons/Close';
import Alert from '../common/Alert';
import { isEmpty } from 'ramda';
import TagsInput from '../common/TagsInput';
import handleKeyDown from '../../utils/handleKeyDown';
import { EDIT_CALENDAR_MODAL  } from '../../actions/types';

const CalendarEditModal = (props) => {

  const [formData, setFormData] = useState({
    name: '',
    description: '',
    color: 1,
    share: []
  });

  const [tags, setTags] = useState([]);

  const selectedTags = tags => {
		setTags(tags)
	};

  const { name, description, share } = formData;

  useEffect(() => {
    if(!isEmpty(props.calendarToEdit)) {
      let share = [];
      if (props.calendarToEdit.share) {
        share = props.calendarToEdit.share.map(user => (
            user.email
          )
        );
      }
      setTags(share);
      setFormData({
        name: props.calendarToEdit.name,
        description: props.calendarToEdit.description,
        color: props.calendarToEdit.color.id,
        share: share
      });
    }
  }, [props.calendarToEdit]);

  const onSubmit = async event => {
    event.preventDefault();

    const newCalendar = {
      name,
      description,
      color: formData.color.toString(),
      share: tags
    };

    await props.updateCalendar(props.calendarToEdit.id, newCalendar);
    props.getCalendars();
  };

  const onChange = event => {
    if (event.target.name === 'color') {
      setFormData({ ...formData, [event.target.name]: parseInt(event.target.value) });
      return;
    };
    setFormData({ ...formData, [event.target.name]: event.target.value });
  }

  return (
    <Modal
      ariaHideApp={false} 
      isOpen={props.isOpen}
      className='Modal'
      overlayClassName='Overlay'
      onRequestClose={() => props.closeModal(EDIT_CALENDAR_MODAL)}
      contentLabel='Example Modal'
    >
      <CloseIcon className='icon-close' onClick={() => props.closeModal(EDIT_CALENDAR_MODAL)} color='secondary' />
      <p className='lead text-center'>
        Edit calendar
      </p>
      <form noValidate onSubmit={event => onSubmit(event)} onKeyDown={handleKeyDown}>
      <Alert notice={props.notice} />
        <TextFieldGroup
          type='text'
          placeholder='Name'
          name='name'
          value={name}
          onChange={event => onChange(event)}
          minLength='2'
          autoComplete='on'
          error={props.errors.name}
        />
        <TextFieldGroup
          type='text'
          placeholder='Description'
          name='description'
          value={description}
          onChange={event => onChange(event)}
          minLength='4'
          autoComplete='on'
          error={props.errors.description}
        />
        <RadioSelect color={formData.color} onChange={onChange} error={props.errors.color}/>
        <p className='lead text-center'>
          Invite People
        </p>
        <TagsInput className='tags' selectedTags={selectedTags} tags={share} onChange={event => onChange(event)}/>
        <input
          type='submit'
          className='btn btn-info btn-modal btn-block mt-4'
        />
      </form>
    </Modal>
  );
}

Modal.propTypes = {
  updateCalendar: PropTypes.func,
  getCalendars: PropTypes.func,
  getEvents: PropTypes.func,
  auth: PropTypes.object,
  errors: PropTypes.object,
  notice: PropTypes.object,
  isOpen: PropTypes.bool.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors,
  notice: state.notice,
  isOpen: state.modal.editCalendarModal
});

export default connect(mapStateToProps, { updateCalendar, getCalendars, getEvents })(CalendarEditModal);
