import React, { useEffect, useState } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import Modal from 'react-modal';
import TextFieldGroup from '../common/TextFieldGroup';
import { addEvent, getEvents} from '../../actions/eventActions';
import CloseIcon from '@material-ui/icons/Close';
import Alert from "../common/Alert";
import { isEmpty } from 'ramda';
import moment from 'moment';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import { ADD_EVENT_MODAL  } from '../../actions/types';

const EventAddModal = (props) => {

  const [formData, setFormData] = useState({
    name: '',
    description: '',
    calendar: '',
    category: '',
    date_from: null,
    date_to: null,
    errors: {}
  });

  const { name, description, calendar, category, date_from, date_to } = formData;
  
  useEffect(() => {
    if(!isEmpty(props.eventData)) {
      setFormData({
        date_from: moment(props.eventData.start).format('YYYY-MM-DDTHH:MM'),
        date_to: moment(props.eventData.end).format('YYYY-MM-DDTHH:MM'),
      });
    }
  }, [props.eventData, props.errors]);

  const getCalendar = () => {
    let calendars = [];
    props.calendars.forEach(calendar => {
        if (calendar.share) {
            calendar.share.forEach(share => {
              if (share.id === props.user.id) {
                calendars.push({value: calendar.id, label: calendar.name, name: 'calendar'});
              }
            }
          )
        }
        if (calendar.owner.id === props.user.id) {
          calendars.push({value: calendar.id, label: calendar.name, name: 'calendar'});
        }
      }
    );
    return calendars;
  }

  const getCategory = () => {
    let Allcategory = [];
    if (props.category) {
      props.category.forEach(category => (
          Allcategory.push({value: category.id, label: category.title, name: 'category'})
        )
      );
    }
    return Allcategory;
  }

  const onSubmit = async event => {
    event.preventDefault();
    const newEvent = {
      name,
      description,
      calendar,
      category,
      date_from: moment(date_from).format('YYYY.MM.DD HH:MM'),
      date_to: moment(date_to).format('YYYY.MM.DD HH:MM')
    };
    await props.addEvent(newEvent)
    props.getEvents(props.calendars.map(calendar => calendar.id), 'Month', 'Today');
  };

  const onChange = event => {
    setFormData({ ...formData, [event.target.name]: event.target.value });
  }
    
  return (
    <Modal
      ariaHideApp={false} 
      isOpen={props.isOpen}
      className='Modal'
      overlayClassName='Overlay'
      onRequestClose={() => props.closeModal(ADD_EVENT_MODAL)}
      contentLabel="Example Modal"
    >
      <CloseIcon className='icon-close' onClick={() => props.closeModal(ADD_EVENT_MODAL)} color='secondary' />
      <p className='lead text-center'>
        Create new event
      </p>
      <form noValidate onSubmit={event => onSubmit(event)}>
      <Alert notice={props.notice} />
        <TextFieldGroup
          type='text'
          placeholder='Name'
          name='name'
          value={name ? name : ''}
          onChange={event => onChange(event)}
          minLength='2'
          autoComplete='on'
          error={props.errors.name}
        />
        <TextFieldGroup
          type='text'
          placeholder='Description'
          name='description'
          value={description ? description : ''}
          onChange={event => onChange(event)}
          minLength='4'
          autoComplete='on'
          error={props.errors.description}
        />
        <div className='flex-column mt-2'>
        <FormControl variant='outlined' className='w-100'>
          <InputLabel id='demo-simple-select-outlined-label'>Calendar</InputLabel>
          <Select
            labelId='demo-simple-select-outlined-label'
            id="demo-simple-select-outlined"
            value={calendar ? calendar : ''}
            name='calendar'
            onChange={event => onChange(event)}
            error={props.errors.calendar && true}
          >
            {
              getCalendar().map((calendar, index) => (
                <MenuItem key={index} value={calendar.value}>{calendar.label}</MenuItem>
              ))
            }
          </Select>
          {props.errors.calendar && <div className='error-text'>{props.errors.calendar}</div>}
        </FormControl>
        </div>
        <div className='flex-column mt-3'>
        <FormControl variant='outlined' className='w-100'>
          <InputLabel id='demo-simple-select-outlined-label'>Category</InputLabel>
          <Select
            labelId='demo-simple-select-outlined-label'
            id="demo-simple-select-outlined"
            value={category ? category : ''}
            name='category'
            onChange={event => onChange(event)}
            error={props.errors.category && true}
          >
            {
              getCategory().map((category, index) => (
                <MenuItem key={index} value={category.value}>{category.label}</MenuItem>
              ))
            }
          </Select>
          {props.errors.category && <div className='error-text'>{props.errors.category}</div>}
        </FormControl>
        </div>
        <div className="mt-4 d-flex justify-content-around">
          <TextField
            id='datetime-local 1'
            label='Time from'
            name='date_from'
            type='datetime-local'
            value={date_from <= date_to ? date_from : date_to}
            onChange={event => onChange(event)}
            InputLabelProps={{
              shrink: true,
            }}
            variant={'outlined'}
          />
          <TextField
            id='datetime-local 2'
            label='Time to'
            name='date_to'
            type='datetime-local'
            value={date_to >= date_from ? date_to : date_from}
            onChange={event => onChange(event)}
            InputLabelProps={{
              shrink: true,
            }}
            variant={'outlined'}
          />
        </div>
        <input
          type='submit'
          className='btn btn-info btn-modal btn-block mt-4'
        />
      </form>
    </Modal>
  );
}

Modal.propTypes = {
  addEvent: PropTypes.func,
  getEvents: PropTypes.func,
  auth: PropTypes.object,
  errors: PropTypes.object,
  notice: PropTypes.object,
  loading: PropTypes.bool,
  isOpen: PropTypes.bool.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors,
  notice: state.notice,
  loading: state.event.loading,
  isOpen: state.modal.addEventModal
});

export default connect(mapStateToProps, { addEvent, getEvents })(EventAddModal);
