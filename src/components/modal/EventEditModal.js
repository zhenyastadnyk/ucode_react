import React, { useEffect, useState } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import Modal from 'react-modal';
import TextFieldGroup from '../common/TextFieldGroup';
import { updateEvent, getEvents, delEvent} from '../../actions/eventActions';
import CloseIcon from '@material-ui/icons/Close';
import Alert from '../common/Alert';
import { isEmpty } from 'ramda';
import TagsInput from '../common/TagsInput';
import TextField from '@material-ui/core/TextField';
import moment from 'moment';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Button from '@material-ui/core/Button';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import { EDIT_EVENT_MODAL  } from '../../actions/types';

const EventEditModal = (props) => {

  const [formData, setFormData] = useState({
    name: '',
    description: '',
    calendar: '',
    category: '',
    invited: [],
    notes: "",
    priority: "",
    date_from: null,
    date_to: null
  });

  const [tags, setTags] = useState([]);

  const selectedTags = tags => {
	  setTags(tags)
  };

  const { name, description, calendar, category, invited, notes, priority, date_from, date_to } = formData;

  useEffect(() => {
    if(!isEmpty(props.eventToEdit)) {
      let invited = [];
      if (props.eventToEdit.invited) {
        invited = props.eventToEdit.invited.map(user => (
            user.email
          )
        );
      }
      setTags(invited);
      setFormData({
        name: props.eventToEdit.title,
        description: props.eventToEdit.description,
        calendar: props.eventToEdit.calendar.id,
        category: props.eventToEdit.category.id,
        invited: invited,
        notes: props.eventToEdit.notes,
        priority: props.eventToEdit.priority,
        date_from: moment(props.eventToEdit.start).format('YYYY-MM-DDTHH:MM'),
        date_to: moment(props.eventToEdit.end).format('YYYY-MM-DDTHH:MM'),
      });
    }
  }, [props.eventToEdit]);

  const onSubmit = async event => {
    event.preventDefault();
    const newEvent = {
      name,
      description,
      calendar,
      category,
      date_from: moment(date_from).format('YYYY.MM.DD HH:MM'),
      date_to: moment(date_to).format('YYYY.MM.DD HH:MM'),
    };

    if (notes) {
      newEvent.notes = notes;
    }
    if (priority) {
      newEvent.priority = priority;
    }
    if (tags) {
      newEvent.invited = tags;
    }

    await props.updateEvent(props.eventToEdit.id, newEvent);
    props.getEvents(props.calendars.map(calendar => calendar.id), 'Month', 'Today');
  };

  const onChange = event => {
    setFormData({ ...formData, [event.target.name]: event.target.value });
  }

  const getCalendar = () => {
    let calendars = [];
    props.calendars.forEach(calendar =>(
        calendars.push({value: calendar.id, label: calendar.name, name: 'calendar'})
      )
    );
    return calendars;
  }

  function getAction(){
    let action = false;
    if (props.eventToEdit.calendar) {
      let id = props.eventToEdit.calendar.id;
      props.calendars.forEach(calendar => {
          if (calendar.id === id && calendar.owner.id === props.user.id) {
            action = true;
          }
        }
      );
    }
    return action;
  }


  const getCategory = () => {
    let Allcategory = [];
    if (props.category) {
      props.category.forEach(category => (
          Allcategory.push({value: category.id, label: category.title, name: 'category'})
        )
      );
    }
    return Allcategory;
  }

  function codeCategory(id){
    let action = "task";
    if (props.category) {
      props.category.forEach(category => {
          if (category.id === id) {
            action = category.code;
          }
        }
      );
    }
    return action;
  }

  const eventDelete = async () => {
    await props.delEvent(props.eventToEdit.id);
    props.getEvents(props.calendars.map(calendar => calendar.id), 'Month', 'Today');
  };

  return (
    <Modal
      ariaHideApp={false} 
      isOpen={props.isOpen}
      className='Modal'
      overlayClassName='Overlay'
      onRequestClose={() => props.closeModal(EDIT_EVENT_MODAL)}
      contentLabel='Example Modal'
    >
      <CloseIcon className='icon-close' onClick={() => props.closeModal(EDIT_EVENT_MODAL)} color='secondary' />
      <p className='lead text-center'>
        Edit event
      </p>
      <form noValidate onSubmit={event => onSubmit(event)} onKeyDown={(event) => {
         if (event.keyCode === 13) {
          event.preventDefault();
         }
      }}>
      <Alert notice={props.notice} />
        <TextFieldGroup
          type='text'
          placeholder='Name'
          name='name'
          value={name}
          onChange={event => onChange(event)}
          minLength='2'
          autoComplete='on'
          error={props.errors.name}
        />
        <TextFieldGroup
          type='text'
          placeholder='Description'
          name='description'
          value={description}
          onChange={event => onChange(event)}
          minLength='4'
          autoComplete='on'
          error={props.errors.description}
        />
        <div className='flex-column mt-2'>
          <FormControl variant='outlined' className='w-100' disabled>
            <InputLabel id='demo-simple-select-disabled-label'>Calendar</InputLabel>
            <Select
              labelId="demo-simple-select-disabled-label"
              id="demo-simple-select-disabled"
              value={calendar ? calendar : ''}
              name='calendar'
              onChange={event => onChange(event)}
            >
              {
                getCalendar().map((calendar, index) => (
                  <MenuItem key={index} value={calendar.value}>{calendar.label}</MenuItem>
                ))
              }
            </Select>
          </FormControl>
        </div>
        <div className='flex-column mt-3'>
          <FormControl variant='outlined' className='w-100' disabled>
            <InputLabel id='demo-simple-select-disabled-label'>Category</InputLabel>
            <Select
              labelId='demo-simple-select-disabled-label'
              id="demo-simple-select-disabled"
              value={category ? category : ''}
              name='category'
              onChange={event => onChange(event)}
            >
              {
                getCategory().map((category, index) => (
                  <MenuItem key={index} value={category.value}>{category.label}</MenuItem>
                ))
              }
            </Select>
          </FormControl>
        </div>
        <div className="mt-4 d-flex justify-content-around">
          <TextField
            id='datetime-local 1'
            label='Time from'
            type='datetime-local'
            name='date_from'
            value={date_from <= date_to ? date_from : date_to}
            onChange={event => onChange(event)}
            InputLabelProps={{
              shrink: true,
            }}
            variant={'outlined'}
          />
          <TextField
            id='datetime-local 2'
            label='Time to'
            type='datetime-local'
            name='date_to'
            value={date_to >= date_from ? date_to : date_from}
            onChange={event => onChange(event)}
            InputLabelProps={{
              shrink: true,
            }}
            variant={'outlined'}
          />
        </div>
        {
          codeCategory(category) === 'arrangement' ?
            (<div><p className='lead text-center'>
            Invite People
          </p>
              < TagsInput selectedTags = {selectedTags} tags={invited}/></div>) : null
        }
        {
          codeCategory(category) === 'task' ?
            (<TextareaAutosize name="notes" value={notes} onChange={event => onChange(event)}
                               aria-label="minimum height" rowsMin={3} placeholder="Notes" />) : null
        }
        {
          codeCategory(category) === 'reminder' ?
            (<div>
              <p className='lead text-center'>
                Priority
              </p>
              <FormControl component="fieldset">
              <RadioGroup aria-label="position" value={priority} name="priority" onChange={event => onChange(event)} row>
              <FormControlLabel
                control={<Radio color={'default'} />}
                name="priority"
                value="high"
                label="High"
              />
              <FormControlLabel
                control={<Radio color={'default'} />}
                name="priority"
                value="average"
                label="Average"
              />
              <FormControlLabel
                control={<Radio color={'default'} />}
                name="priority"
                value="low"
                label="Low"
              />
              </RadioGroup>
              </FormControl>
            </div>)
            : null
        }
        <input
          type='submit'
          className='btn btn-info btn-modal btn-block mt-4'
        />
        <div className="text-center btn btn-block">
          <Button variant="contained" color="secondary" disabled={!getAction()} onClick={eventDelete}>
            Delete
          </Button>
        </div>
      </form>
    </Modal>
  );
}

Modal.propTypes = {
  updateEvent: PropTypes.func,
  getEvents: PropTypes.func,
  delEvent: PropTypes.func,
  auth: PropTypes.object,
  errors: PropTypes.object,
  notice: PropTypes.object,
  isOpen: PropTypes.bool.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors,
  notice: state.notice,
  isOpen: state.modal.editEventModal
});

export default connect(mapStateToProps, { updateEvent, getEvents, delEvent })(EventEditModal);
