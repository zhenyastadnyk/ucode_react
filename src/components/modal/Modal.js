import React from 'react';
import CalendarAddModal from '../modal/CalendarAddModal';
import CalendarEditModal from '../modal/CalendarEditModal';
import EventEditModal from '../modal/EventEditModal';
import EventAddModal from '../modal/EventAddModal';

const ModalWindow = (props) => {

  return (
    <div>
      <CalendarAddModal
        colors={props.colors}
        closeModal={props.closeModal}
      />
      <CalendarEditModal
        colors={props.colors}
        closeModal={props.closeModal}
        calendarToEdit={props.calendarToEdit}
        calendars={props.calendars}
      />
      <EventAddModal
        category={props.category}
        calendars = {props.calendars}
        closeModal={props.closeModal}
        eventData={props.eventData}
        user={props.user}
      />
      <EventEditModal
        category={props.category}
        calendars = {props.calendars}
        closeModal={props.closeModal}
        eventToEdit={props.eventToEdit}
        user={props.user}
      />
    </div>
  );
}

export default ModalWindow;
