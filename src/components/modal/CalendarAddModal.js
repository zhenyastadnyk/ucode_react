import React, { useEffect, useState } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import Modal from 'react-modal';
import TextFieldGroup from '../common/TextFieldGroup';
import { addCalendar, getCalendars} from '../../actions/calendarActions';
import RadioSelect from '../common/RadioSelect';
import CloseIcon from '@material-ui/icons/Close';
import Alert from "../common/Alert";
import handleKeyDown from '../../utils/handleKeyDown';
import { ADD_CALENDAR_MODAL  } from '../../actions/types';

const CalendarAddModal = (props) => {

  const [formData, setFormData] = useState({
    name: '',
    description: '',
    color: 1,
    owner: props.auth.user.id,
    errors: {},
    loading: false
  });
  
  const { name, description, color } = formData;
  
  useEffect(() => {
    setFormData({errors: props.errors, loading: props.calendarsLoading})
  }, [props.errors, props.calendarsLoading]);

  const onSubmit = async event => {
    event.preventDefault();
    const newCalendar = {
      name,
      description,
      color: color ? color : 1,
      owner: 3
    };
    await props.addCalendar(newCalendar);
    props.getCalendars();
  };

  const onChange = event => {
    if (event.target.name === 'color') {
      setFormData({ ...formData, [event.target.name]: parseInt(event.target.value) });
      return;
    };
    
    setFormData({ ...formData, [event.target.name]: event.target.value });
  }
    

  return (
    <Modal
      ariaHideApp={false} 
      isOpen={props.isOpen}
      className='Modal'
      overlayClassName='Overlay'
      onRequestClose={() => props.closeModal(ADD_CALENDAR_MODAL)}
      contentLabel="Example Modal"
    >
      <CloseIcon className='icon-close' onClick={() => props.closeModal(ADD_CALENDAR_MODAL)} color='secondary' />
      <p className='lead text-center'>
        Create new calendar
      </p>
      <form noValidate onSubmit={event => onSubmit(event)} onKeyDown={handleKeyDown}>
      <Alert notice={props.notice} />
        <TextFieldGroup
          type='text'
          placeholder='Name'
          name='name'
          value={name ? name : ''}
          onChange={event => onChange(event)}
          minLength='2'
          autoComplete='on'
          error={props.errors.name}
        />
        <TextFieldGroup
          type='text'
          placeholder='Description'
          name='description'
          value={description ? description : ''}
          onChange={event => onChange(event)}
          minLength='4'
          autoComplete='on'
          error={props.errors.description}
        />
        <RadioSelect color={color ? color : 1} onChange={onChange} error={props.errors.color}/>
        <input
          type='submit'
          className='btn btn-info btn-modal btn-block mt-4'
        />
      </form>
    </Modal>
  );
}

Modal.propTypes = {
  addCalendar: PropTypes.func.isRequired,
  getCalendars: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
  notice: PropTypes.object.isRequired,
  calendarsLoading: PropTypes.bool.isRequired,
  isOpen: PropTypes.bool.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors,
  notice: state.notice,
  calendarsLoading: state.calendar.loading,
  isOpen: state.modal.addCalendarModal
});

export default connect(mapStateToProps, { addCalendar, getCalendars })(CalendarAddModal);
