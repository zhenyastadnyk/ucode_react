import React from 'react';
import loading from '../../img/ring-loader.gif';

const Spinner = (props) => {
  return (
    <div>
      <img
        src={loading}
        style={props.style || {
          width: '20%',
          display: 'block',
          marginLeft: 'auto',
          marginRight: 'auto'
        }}
        alt='Loading...'
      />
    </div>
  );
}

export default Spinner;