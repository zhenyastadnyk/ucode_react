import React from 'react';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';

const RadioSelect = (props) => {
  return (
    <div>
      <p className='lead text-center'>
        Color:
      </p>
      <RadioGroup className='colors' value={props.color} onChange={props.onChange}>
        {
          props.colors.map((color, index) => (
            <div className='colors' key={index}>
              <FormControlLabel name="color" style={{color: color.code}} value={index + 1} control={<Radio color={'default'} />} label={color.title} />
            </div>
          ))
        }
      </RadioGroup>
      {props.error && <p className='invalid-feedback d-block'>{props.error}</p>}
    </div>
  );
}

RadioSelect.propTypes = {
  colors: PropTypes.array.isRequired
};

const mapStateToProps = state => ({
  colors: state.calendar.colors
});

export default connect(
  mapStateToProps
)(RadioSelect);
