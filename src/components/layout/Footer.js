import React from 'react';

const Footer = () => {
  return (
    <div>
      <div className='footer-margin'></div>
      <footer className='row footer bg-dark text-white mt-5 p-4 text-center navbar-bottom'>
        Copyright &copy; {new Date().getFullYear()} Chronos
      </footer>
    </div>
  );
}

export default Footer;
