import React, { useState, useEffect } from 'react';
import {
  ADD_CALENDAR_MODAL,
  EDIT_CALENDAR_MODAL,
  ADD_EVENT_MODAL,
  EDIT_EVENT_MODAL
} from '../../actions/types';
import { connect } from 'react-redux';
import ModalWindow from '../modal/Modal';
import PropTypes from 'prop-types';
import BigCalendar from './BigCalendar'
import SmallCalendar from './SmallCalendar'
import { getCalendars } from '../../actions/calendarActions';
import { delCalendar } from '../../actions/calendarActions';
import { getColors } from '../../actions/calendarActions';
import { getCategory } from '../../actions/eventActions';
import { eventSearch } from '../../actions/eventActions';
import { setMessage } from '../../actions/messageAction';
import { getEvents} from '../../actions/eventActions';
import { isEmpty } from 'ramda';
import { makeStyles } from '@material-ui/core/styles';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { green } from '@material-ui/core/colors';
import Icon from '@material-ui/core/Icon';
import Spinner from '../common/Spinner';
import TextFieldGroup from '../common/TextFieldGroup';
import { modalAction } from '../../actions/modalAction';
import Alert from '../common/Alert';
import moment from 'moment';

const Calendar = (props) => {

  const [state, setState] = useState({
    calendarToEdit: {},
    eventToEdit: {},
    eventData: {},
  });

  const [calendarsState, setCalendarsState] = useState([]);

  const [dateState, setDateState] = useState({
      date: new Date()
  });

  const [categoryState, setCategoryState] = useState([]);

  const { date } = dateState;

  const [viewState, setViewState] = useState({
      view: 'month'
  });

    const { view } = viewState;

  const [searchState, setSearchState] = useState('');

  const {
    getCalendars,
    calendars,
    getColors,
    eventSearch,
    search_event,
    colors,
    delCalendar,
    user,
    loading,
    getCategory,
    category,
    calendarsLoading,
  } = props;

  const updateDate = date => {
    setDateState({ date });
    props.getEvents(getActive(), 'Month', date.toString().substring(4,24));
  };

  const updateView = view => {
    setViewState({view})
  }

  const useStyles = makeStyles(theme => ({
      root: {
          display: 'flex',
      },
      formControl: {
          margin: theme.spacing(3),
      },
  }));
  const classes = useStyles();

  const handleChange = id => event => {
    let tempCalendars = [];
    calendarsState.forEach(calendar => {
      if (calendar.id === id) {
        calendar.isChecked = event.target.checked;
      }
      tempCalendars.push(calendar)
    });
    setCalendarsState(tempCalendars)
  };

  const handleCategoryChange = id => event => {
    let tempCategory = [];
    categoryState.forEach(category => {
      if (category.id === id) {
        category.isChecked = event.target.checked;
      }
      tempCategory.push(category)
    });
    setCategoryState(tempCategory)
  };

  const calendarDelete = async (id) => {
      await delCalendar(id);
      getCalendars();
  };


  useEffect(() => {
    if (isEmpty(calendars)) {
      getCalendars();
      getColors();
      getCategory();
    } else {
      let tempCalendars = [];
      let tempCategory = [];
      calendars.forEach(calendar => {
        calendar.isChecked = true;
        tempCalendars.push(calendar)
      });
      category.forEach(cat => {
        cat.isChecked = true;
        tempCategory.push(cat)
      });
      setCalendarsState(tempCalendars);
      setCategoryState(tempCategory);
    }

  }, [calendars, getCalendars, getColors, getCategory])

  const getActive = () => {
    let arid = [];
    calendarsState.forEach(calendar => {
      if (calendar.isChecked) {
        arid.push(calendar.id);
      }
    });
    return arid;
  }

  const getActiveCategory = () => {
    let arid = [];
    categoryState.forEach(category => {
      if (category.isChecked) {
        arid.push(category.id);
      }
    });
    return arid;
  }
  
  const openModal = (type) => {
    props.modalAction(type, true);
  }

  const closeModal = (type) => {
    props.modalAction(type, false);
  }

  const handleEditCalendar = (calendarToEdit) => {
    setState({ ...state, calendarToEdit });
    openModal(EDIT_CALENDAR_MODAL);
  }

  const handleEditEvent = (eventToEdit) => {
    if (eventToEdit.category && eventToEdit.category.code !== 'holiday') {
      setState({ ...state, eventToEdit });
      openModal(EDIT_EVENT_MODAL);
    }
    else {
      props.setMessage('warning', `You don't have permission to edit this event.`);
    }
  }

  const handleAddEvent = (eventData) => {
    setState({ ...state, eventData });
    openModal(ADD_EVENT_MODAL);
  }

  if (loading) {
    return <Spinner/>;
  }

  const valueSearch = async event => {
    setSearchState(event.target.value)
    if (event.target.value.length > 2 && event.target.value.length < 15) {
      await eventSearch(getActive(), event.target.value);
    }
    document.getElementById('search_result').hidden = 0;
  }


  return (
    <div className='calendar'>
      <div className='col-md-8 m-auto'>
        <Alert notice={props.notice} />
      </div>
      <div className='calendar row'>
        <div className='small col-md-4'>
          <SmallCalendar updateDate={updateDate} />
            <FormControl component='fieldset' className={classes.formControl}>
            {
              calendarsLoading ? 
            (<div className='d-flex'>
                <FormLabel className='d-flex' component='legend'>Calendars</FormLabel>
                <Spinner style={{width:'20%'}}/>
              </div>) :
            (<FormLabel component='legend'>Calendars</FormLabel>)
            }
            
            <FormGroup className="calendar-list">
              {
                calendarsState.map((calendar, index) => (
                  <div className='calendar-items' key={index}>
                    <FormControlLabel
                        control={<Checkbox style={{color: calendar.color.code}} checked={calendar.isChecked}
                                            onChange={handleChange(calendar.id)} value={calendar.id} />}
                        label={`${calendar.name}`}
                    />
                    { calendar.owner.id === user.id ?
                      (
                        <span>
                          <Icon
                            className='fa fa-edit'
                            variant='primary'
                            style={{fontSize: 20}}
                            onClick={() => handleEditCalendar(calendar)}
                          />
                          <Icon
                            className='fa fa-trash'
                            onClick={() => calendarDelete(calendar.id)}
                            color='secondary'
                            style={{fontSize: 20}}
                          />
                        </span>
                      ) : null
                    }
                  </div>
                ))
              }
            </FormGroup>
            </FormControl>
            <div className={classes.root}>
              <Icon className='fa fa-plus-circle' onClick={() => openModal(ADD_CALENDAR_MODAL)} style={{ color: green[400], fontSize: 40}} />
              
            </div>
            <ModalWindow
              colors={colors}
              category={category}
              calendars={calendarsState}
              closeModal={closeModal}
              calendarToEdit={state.calendarToEdit}
              eventToEdit={state.eventToEdit}
              eventData={state.eventData}
              user={user}
            />
        </div>
        <div className='col-md-8'>
          <div className='row'>
            <div className='col-md-6'>
              <div className='search'>
                <TextFieldGroup
                    type='text'
                    placeholder='Search'
                    name='search'
                    value={searchState}
                    onChange={event => valueSearch(event)}
                    minLength='2'
                    autoComplete='on'
                />
              </div>
              <div id='search_result' className='search_result'>
                {
                  searchState.length > 2 ?
                    search_event.length ?
                      (search_event.map((events, index) => (
                        <div className="search_point" key={index} onClick={() =>
                        { document.getElementById('search_result').hidden = 1; setViewState({view: 'day'});
                            updateDate(moment(events.start).toDate())}}>{events.title} ({events.start})
                        </div>)
                      )) :
                      <div>no events found</div>
                    :
                     null
                }
              </div>
            </div>
            <div className='col-md-6'>
              <FormGroup className="category-list">
              {
                categoryState.map((category, index) => (
                  <span key={index}>
                    <FormControlLabel
                      control={<Checkbox checked={category.isChecked}
                                         onChange={handleCategoryChange(category.id)} value={category.id} />}
                      label={`${category.title}`}
                    />
                  </span>
                ))
              }
              </FormGroup>
            </div>
          </div>
          <BigCalendar 
            calendars = {getActive()}
            categories = {getActiveCategory()}
            onAddEvent={(event) => handleAddEvent(event)}
            onEditEvent={(event) => handleEditEvent(event)}
            updateDate={updateDate}
            date={date}
            updateView={updateView}
            view={view}
          />
        </div>
          <div>
        </div>
      </div>
    </div>
  );
};

Calendar.propTypes = {
  getCalendars: PropTypes.func.isRequired,
  getColors: PropTypes.func.isRequired,
  getCategory: PropTypes.func.isRequired,
  eventSearch: PropTypes.func.isRequired,
  getEvents: PropTypes.func,
  modalClose: PropTypes.func,
  user: PropTypes.object,
  calendarsLoading: PropTypes.bool.isRequired,
  modalAction: PropTypes.func.isRequired,
  setMessage: PropTypes.func.isRequired,
  notice: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  loading: state.auth.loading,
  calendarsLoading: state.calendar.loading,
  calendars: state.calendar.calendars,
  colors: state.calendar.colors,
  category: state.event.category,
  search_event: state.event.search_event,
  user: state.auth.user,
  notice: state.notice
});

export default connect(
mapStateToProps, {
    getCalendars,
    getColors,
    delCalendar,
    getCategory,
    eventSearch,
    getEvents,
    modalAction,
    setMessage
})(Calendar);
