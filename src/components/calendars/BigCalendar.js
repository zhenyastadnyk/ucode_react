import React, {useEffect, useState} from 'react';
import {
  Calendar, momentLocalizer
} from 'react-big-calendar';
import moment from 'moment';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import { getEvents } from '../../actions/eventActions';
import PropTypes from "prop-types";
import {connect} from "react-redux";

const localizer = momentLocalizer(moment); // or globalizeLocalizer

const BigCalendar = (props) => {
  let [eventsState, setEventsState] = useState({
    events: [],
  });
  let [calendarsState, setCalendarsState] = useState({
    calendars: [],
  });

  let [onViewState, setOnViewState] = useState({
    onView: 'Month',
  });

  let [onNavigateState, setOnNavigateState] = useState({
    onNavigate: 'Today',
  });

  const { events } = eventsState;
  const { onView,} = onViewState;
  const { onNavigate } = onNavigateState;

  useEffect(() => {
    if ((props.calendars.length !== calendarsState.calendars.length || eventsState.events.length === 0)
      && props.calendars.length) {
      props.getEvents(props.calendars, onView ? onView : 'Month', onNavigate ? onNavigate : 'Today');
      setEventsState({events: props.events})
      setCalendarsState({calendars: props.calendars})
    }
  }, [events, props.calendars.length, onNavigate, onView, props, calendarsState.calendars.length, eventsState.events.length]);

  const getEventsWithParams = (navigate, view) =>  {
    if (view && view !== 'agenda') {
      setOnViewState({onView: view});
      props.updateView(view);
      props.updateDate(onNavigate && onNavigate !== 'Today' ? moment(onNavigate).toDate() : props.date);
      props.getEvents(props.calendars, view, onNavigate ? onNavigate : 'Today');
    } else if (navigate) {
      let formatNavigate = (typeof navigate === 'object')
        ? navigate.toString().substring(4,24)
        : navigate;

      props.updateDate(navigate);
      setOnNavigateState({onNavigate: formatNavigate});
      props.getEvents(props.calendars, onView, formatNavigate);
    }
  };

  const getEventsWithCorrectDates = events => {
    for (let i = 0; i < events.length; i++) {
      events[i].start = moment(events[i].start).toDate();
      events[i].end = moment(events[i].end).toDate();
    }

    return events;
  };

  const setEventCategoryIcon = event => {
    switch (event.category.code) {
      case 'holiday':
        return 'fas fa-mug-hot';
      case 'reminder':
        return 'far fa-bell';
      case 'task':
        return 'fa fa-tasks';
      case 'arrangement':
        return 'fas fa-clipboard-list';
      default:
        return null;
    }
  };

  const setEventColor = event => {
    return {
      style: {
        backgroundColor: (event.calendar.color.code),
      },
      className: (setEventCategoryIcon(event))
    }
  };

  const getEventsFilterCategories = events => {
    return events.filter(event => props.categories.includes(event.category.id));
  };

  return (
    <div>
      <Calendar
        selectable
        localizer={localizer}
        events={getEventsFilterCategories(getEventsWithCorrectDates(props.events))}
        startAccessor='start'
        endAccessor='end'
        style={{height: 700, flex: 1}}
        onView={(event) => getEventsWithParams(null, event)}
        onNavigate={(event) => getEventsWithParams(event, null)}
        onSelectEvent={(event) => props.onEditEvent(event)}
        onSelectSlot={(event) => props.onAddEvent(event)}
        longPressThreshold={1}
        views={{ month: true, week: true, day: true }}
        eventPropGetter={event => setEventColor(event)}
        date={moment(props.date).toDate()}
        view={props.view}
      />
    </div>
  );
};

getEvents.propTypes = {
  getEvents: PropTypes.func,
  events: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  events: state.event.events,
  onNavigate: state.onNavigate,
  onView: state.onView,
  // calendars: state.calendars
});

export default connect(
    mapStateToProps,
    { getEvents }
)(BigCalendar);