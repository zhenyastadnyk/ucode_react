import React, { useState } from 'react';

import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';

const SmallCalendar = (props) => {
  let [dateState, setDateState] = useState({
      date:  new Date(),
  });

  const { date } = dateState;

  const onChange = date => {
    setDateState({date: date});
    props.updateDate(date);
  };

    return (
      <div>
        <Calendar
          onChange={onChange}
          value={date}
        />
      </div>
    );
}

export default SmallCalendar;
