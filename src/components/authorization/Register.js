import React, { Fragment, useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { registerUser, resendEmail } from '../../actions/authActions';
import PropTypes from 'prop-types';
import Alert from '../common/Alert';
import Spinner from '../common/Spinner';
import TextFieldGroup from '../common/TextFieldGroup';
import { Link } from 'react-router-dom';

const Register = ({ registerUser, errors, notice, location, resendEmail }) => {
  
  const [formData, setFormData] = useState({
    name: '',
    email: '',
    password: '',
    password_confirmation: '',
    isEmailSent: false
  });

  const { name, email, password, password_confirmation, isEmailSent } = formData;

  useEffect(() => {
    if (notice.success) {
      setFormData({
        name: '',
        email: '',
        password: '',
        password_confirmation: '',
        errors: {},
        notice: {}
      });
    }
  }, [notice.success])

  useEffect(() => {
    if (location.state && location.state.resetPage) {
      setFormData({
        isEmailSent: false
      });
    }
  }, [location.state])

  const onChange = event =>
    setFormData({ ...formData, [event.target.name]: event.target.value });

  const onSubmit = event => {
    event.preventDefault();

    const invitedToEvent = new URL(window.location.href).searchParams.get('event');

    registerUser({ name: name ? name : "", email: email ? email : "", password: password ? password : "",
        password_confirmation, event: invitedToEvent ? invitedToEvent : '' });
  };

  const handleResendEmail = event => {
    event.preventDefault();

    resendEmail(email);
  };

  if (isEmailSent) {
    return (
      <Fragment>
        <div className='register'>
          <div className='container'>
            <div className='row'>
              <div className='col-md-8 m-auto'>
                {notice.loading && !Object.entries(errors).length && (
                  <Spinner />
                )}
                <Alert notice={notice} />
                <h1 className='display-4 text-center'>Resend activation email</h1>
                <p className='lead text-center'>Proceed with caution.</p>
                <form noValidate onSubmit={event => onSubmit(event)}>
                  <TextFieldGroup
                    type='email'
                    placeholder='Email Address'
                    name='email'
                    value={email ? email : ''}
                    onChange={event => onChange(event)}
                    autoComplete='email'
                    error={errors.email || errors.general}
                  />
                  <input
                    type='submit'
                    className='btn btn-info btn-block mt-4'
                    onClick={event => handleResendEmail(event)}
                  />
                  <p className='my-1 mt-4'>
                    Got here by mistake?
                    <Link 
                      to='#'
                      className='link-button ml-2'
                      onClick={() => setFormData({isEmailSent: false})}>
                        Back to registration form
                    </Link>
                  </p>
                </form>
              </div>
            </div>
          </div>
        </div>
    </Fragment>
    )
  }

  return (
    <Fragment>
      <div className='register'>
        <div className='container'>
          <div className='row'>
            <div className='col-md-8 m-auto'>
              {notice.loading && !Object.entries(errors).length && (
                <Spinner />
              )}
              <Alert notice={notice} />
              <h1 className='display-4 text-center'>Sign Up</h1>
              <p className='lead text-center'>Create your Matches account</p>
              <form noValidate onSubmit={event => onSubmit(event)}>
                <TextFieldGroup
                  type='text'
                  placeholder='Name'
                  name='name'
                  value={name ? name : ''}
                  onChange={event => onChange(event)}
                  autoComplete='name'
                  error={errors.name}
                />
                <TextFieldGroup
                  type='email'
                  placeholder='Email Address'
                  name='email'
                  value={email ? email : ''}
                  onChange={event => onChange(event)}
                  autoComplete='email'
                  error={errors.email}
                />
                <TextFieldGroup
                  type='password'
                  placeholder='Password'
                  name='password'
                  value={password ? password : ''}
                  onChange={event => onChange(event)}
                  autoComplete='current-password'
                  error={errors.password}
                />
                <TextFieldGroup
                  type='password'
                  placeholder='Confirm Password'
                  name='password_confirmation'
                  value={password_confirmation ? password_confirmation : ''}
                  onChange={event => onChange(event)}
                  autoComplete='current-password'
                  error={errors.password_confirmation || errors.general}
                />
                <p className='my-1 mt-4'>
                    Where is my email?
                  <Link 
                    to='#'
                    className='link-button ml-2'
                    onClick={() => setFormData({isEmailSent: true})}>
                      Resend
                  </Link>
                </p>
                <input
                  type='submit'
                  className='btn btn-info btn-block mt-4'
                />
              </form>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

Register.propTypes = {
  registerUser: PropTypes.func.isRequired,
  resendEmail: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
  notice: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors,
  notice: state.notice
});

export default connect(
  mapStateToProps,
  { registerUser, resendEmail }
)(Register);
