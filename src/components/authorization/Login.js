import React, { Fragment, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { loginUser, confirmUser } from '../../actions/authActions';
import TextFieldGroup from '../common/TextFieldGroup';
import { Link, Redirect } from 'react-router-dom';
import Alert from '../common/Alert';
import { isEmpty } from 'ramda';
import Spinner from '../common/Spinner';

const Login = ({loginUser, errors, notice, confirmUser, isAuthenticated, loading}) => {
  
  const [formData, setFormData] = useState({
    email: '',
    password: '',
    isVerified: false
  });

  const { email, password, isVerified } = formData;

  useEffect(() => {
    if (notice.success) {
      setFormData({
        email: '',
        password: ''
      });
    }
  }, [notice.success])

  useEffect(() => {
    const { pathname, search } = window.location;

    if (!isEmpty(pathname) && !isEmpty(search) && !isVerified) {
      setFormData({
        isVerified: true,
        email: '',
        password: ''
      });
      confirmUser({ pathname, search });
    }
  }, [isVerified, confirmUser])

  const onChange = event =>
    setFormData({ ...formData, [event.target.name]: event.target.value });

  const onSubmit = event => {
    event.preventDefault();

    loginUser({email, password});
  };

  if (loading) {
    return <Spinner/>;
  }

  if (isAuthenticated && !loading) {
    return <Redirect to='/calendar' />;
  }

  return (
    <Fragment>
      <div className='login'>
        <div className='container'>
          <div className='row'>
            <div className='col-md-8 m-auto'>
              <Alert notice={notice} />
              <h1 className='display-4 text-center'>Log In</h1>
              <p className='lead text-center'>
                Sign in to your Chronos account
              </p>
              <form noValidate onSubmit={event => onSubmit(event)}>
                <TextFieldGroup
                  type='email'
                  placeholder='Email Address'
                  name='email'
                  value={email}
                  onChange={event => onChange(event)}
                  autoComplete='email'
                  error={errors.email || errors.verification}
                />
                <TextFieldGroup
                  type='password'
                  placeholder='Password'
                  name='password'
                  value={password}
                  onChange={event => onChange(event)}
                  minLength='4'
                  autoComplete='current-password'
                  error={errors.password || errors.general}
                />
                <Link to='/reset-password'>Forgot your password?</Link>
                <input
                  type='submit'
                  className='btn btn-info btn-block mt-4'
                />
              </form>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
}

Login.propTypes = {
  loginUser: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
  errors: PropTypes.object.isRequired,
  notice: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated,
  loading: state.auth.loading,
  errors: state.errors,
  notice: state.notice
});

export default connect(
  mapStateToProps,
  { confirmUser, loginUser }
)(Login);
