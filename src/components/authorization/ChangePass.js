import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { changePass } from '../../actions/authActions';
import TextFieldGroup from '../common/TextFieldGroup';
import { isEmpty } from 'ramda';
import { Link } from 'react-router-dom';
import Alert from '../common/Alert';

const ChangePass = ({errors, notice, changePass}) => {

  const [formData, setFormData] = useState({
    password: '',
    password2: ''
  });

  const { password, password2 } = formData;

  useEffect(() => {
    if (notice.success) {
      setFormData({
        password: '',
        password2: ''
      });
    }
  }, [notice.success]);

  const onChange = event =>
    setFormData({ ...formData, [event.target.name]: event.target.value });

  const onSubmit = event => {
    event.preventDefault();

    const pathnameArray = window.location.pathname.split('/');
    const pathname = pathnameArray[pathnameArray.length - 1];
    const search = new URL(window.location.href).searchParams.get('email');
    
    if (!isEmpty(pathname) && !isEmpty(search)) {
      const userData = {
        email: search,
        token: pathname,
        password,
        password_confirmation: password2
      };
      changePass(userData);
    }
  };

  return (
    <div className='activation'>
      <div className='container'>
        <div className='row'>
          <div className='col-md-8 m-auto'>
            <Alert notice={notice} />
            <h1 className='display-4 text-center'>Password reset</h1>
            <p className='lead text-center'>Please choose new password</p>
            <form noValidate onSubmit={event => onSubmit(event)}>
              <TextFieldGroup
                type='password'
                placeholder='New password'
                name='password'
                value={password}
                onChange={event => onChange(event)}
                minLength='4'
                autoComplete='current-password'
                error={errors.password}
              />
              <TextFieldGroup
                placeholder='Confirm new password'
                name='password2'
                type='password'
                value={password2}
                onChange={event => onChange(event)}
                minLength='4'
                autoComplete='current-password'
                error={errors.password2 || errors.general}
              />
              <p className='my-1 mt-4'>
                <Link 
                  to='/login'
                  className='link-button ml-2'
                >
                  Back to login
                </Link>
              </p>
              <input
                type='submit'
                className='btn btn-info btn-block mt-4'
              />
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

ChangePass.propTypes = {
  changePass: PropTypes.func.isRequired,
  errors: PropTypes.object.isRequired,
  notice: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  errors: state.errors,
  notice: state.notice
});

export default connect(
  mapStateToProps,
  { changePass }
)(ChangePass);
