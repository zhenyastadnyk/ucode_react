import React, { Fragment, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { resetPass } from '../../actions/authActions';
import TextFieldGroup from '../common/TextFieldGroup';
import Alert from '../common/Alert';
import Spinner from '../common/Spinner';

const ResetPass = ({auth, notice, errors, resetPass}) => {

  const [formData, setFormData] = useState({
    email: '',
    password: ''
  });

  const { email } = formData;

  useEffect(() => {
    if (notice.success) {
      setFormData({
        email: '',
        password: ''
      });
    }
  }, [notice.success])

  const onChange = (event) => {
    setFormData({ ...formData, [event.target.name]: event.target.value });
  }

  const onSubmit = event => {
    event.preventDefault();
    
    resetPass({email});
  };

  if (auth.isAuthenticated) {
    return <Redirect to='/calendar' />
  }
  
  return (
    <Fragment>
      <div>
        <div className='reset-pass'>
          <div className='container'>
            <div className='row'>
              <div className='col-md-8 m-auto'>
                {
                  notice.loading &&
                  !Object.entries(errors).length &&
                  <Spinner />
                }
                <Alert notice={notice} />
                <h3 className='display-4 text-center'>Forgot password?</h3>
                <p className='lead text-center'>
                  Enter your email for password reset
                </p>
                <form noValidate onSubmit={event => onSubmit(event)}>
                  <TextFieldGroup
                    type='email'
                    placeholder='Email Address'
                    name='email'
                    value={email}
                    onChange={event => onChange(event)}
                    autoComplete='email'
                    error={errors.email}
                  />
                  <input
                    type='submit'
                    className='btn btn-info btn-block mt-4'
                  />
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
}

ResetPass.propTypes = {
  resetPass: PropTypes.func.isRequired,
  errors: PropTypes.object.isRequired,
  notice: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors,
  notice: state.notice
});

export default connect(
  mapStateToProps,
  { resetPass }
)(ResetPass);
