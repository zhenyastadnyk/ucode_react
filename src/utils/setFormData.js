const formData = params => {
    const body = new FormData();
    for (var key in params) {    
      body.set(`${key}`, params[key]);
    }
    return body;
  };
  
  export default formData;
  