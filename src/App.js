import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './store';
import jwt_decode from 'jwt-decode';
import setAuthToken from './utils/setAuthToken';
import { setCurrentUser, logoutUser } from './actions/authActions';
import PrivateRoute from "./components/common/PrivateRoute";

import Navbar from './components/layout/Navbar';
import Footer from './components/layout/Footer';
import Landing from './components/layout/Landing';
import Register from './components/authorization/Register';
import Login from './components/authorization/Login';
import ResetPass from './components/authorization/ResetPass';
import ChangePass from './components/authorization/ChangePass';
import NotFound from './components/not-found/NotFound';
import axios from 'axios';

import Calendar from './components/calendars/Calendar';

import './App.css';

axios.defaults.headers.common['Accept'] = 'application/json';

if (localStorage.jwtToken) {
  setAuthToken(localStorage.jwtToken);
  const decoded = jwt_decode(localStorage.jwtToken);
  store.dispatch(setCurrentUser());
  const currentTime = Date.now() / 1000;
  
  if (decoded.exp < currentTime) {
    store.dispatch(logoutUser());
    window.location.href = '/login';
  }
}

const App = () => {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <div className='App'>
          <Navbar />
          <Switch>
            <Route exact path='/' component={Landing} />
              <Route exact path='/register' component={Register} />
              <Route exact path='/login' component={Login} />
              <Route path='/auth/confirm' component={Login} />
              <Route exact path='/reset-password' component={ResetPass} />
              <Route path='/auth/reset' component={ChangePass} />
              <PrivateRoute exact path='/calendar' component={Calendar} />
              <Route component={NotFound} />
          </Switch>
          <Footer />
        </div>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
