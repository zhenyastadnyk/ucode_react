import axios from 'axios';
import {
  GET_EVENTS,
  EVENT_LOADING,
  GET_CATEGORY,
  UPDATE_EVENT,
  DEL_EVENT,
  GET_EVENTS_SEARCH,
  ADD_EVENT_MODAL,
  EDIT_EVENT_MODAL
} from './types';
import { isEmpty } from 'ramda';
import { validateAddEvent, validateEditEvent } from '../validation';
import formData from '../utils/setFormData';
import { setMessage } from './messageAction';

const setCalendarsQuery = calendars => {
  let calendarsQuery = '';
  calendars.forEach(calendar => {
    calendarsQuery += 'calendar_ids[]=' + calendar + '&';
  });

  return calendarsQuery;
};


export const addEvent = (eventData) => dispatch => {
  dispatch(setEventLoading(true));

  const { errors } = validateAddEvent(eventData);
  if (!isEmpty(errors)) {
    dispatch(setMessage('warning', errors));
    dispatch(dispatch(setEventLoading(false)));
    return;
  }

  axios.defaults.headers['X-CALENDAR-TOKEN'] = process.env.REACT_APP_CALENDAR_SECRET;
  
  const body = formData(eventData);
  axios
    .post(`${process.env.REACT_APP_HOST}/event/add`, body)
    .then(() =>
      dispatch({ type: ADD_EVENT_MODAL, payload: false })
    )
    .catch(err => {
      dispatch(setMessage('warning', err.response.data.message));
    }
  );

  dispatch(setEventLoading(false));
};

// Update event
export const updateEvent = (id, eventData) => dispatch => {
  dispatch(setEventLoading(true));

  const { errors } = validateEditEvent(eventData);
  if (!isEmpty(errors)) {
    dispatch(setMessage('warning', errors));
    dispatch(dispatch(setEventLoading(false)));
    return;
  }

  axios.defaults.headers['X-CALENDAR-TOKEN'] = process.env.REACT_APP_CALENDAR_SECRET;

  axios
    .put(`${process.env.REACT_APP_HOST}/event/${id}`, eventData)
    .then(res =>
      dispatch({ type: UPDATE_EVENT, payload: res.data })
    )
    .then(() =>
      dispatch({ type: EDIT_EVENT_MODAL, payload: false })
    )
    .catch(err => {
      dispatch(setMessage('warning', err.response.data.message));
    }
  );

  dispatch(setEventLoading(false));
};


// Get events
export const getEvents = (calendars, event, navigation) => dispatch => {
  dispatch(setEventLoading(true));
  axios.defaults.headers['X-CALENDAR-TOKEN'] = process.env.REACT_APP_CALENDAR_SECRET;

  const calendarsQuery = setCalendarsQuery(calendars);
  axios
    .get(`${process.env.REACT_APP_HOST}/event/all/get?${calendarsQuery}period=${event}&navigation=${navigation}`)
    .then(res =>
      dispatch({ type: GET_EVENTS, payload: res.data })
    )
    .catch(err =>
      dispatch(setMessage('warning', err.response.data.message))
    );

  dispatch(setEventLoading(false));
};

// Get events search
export const eventSearch = (calendars, search) => dispatch => {
  dispatch(setEventLoading(true));
  const calendarsQuery = setCalendarsQuery(calendars);
  axios
    .get(`${process.env.REACT_APP_HOST}/event/search/get?${calendarsQuery}search=${search}`)
    .then(res =>
      dispatch({ type: GET_EVENTS_SEARCH, payload: res.data })
    )
    .catch(err =>
      dispatch(setMessage('warning', err.response.data.message))
    );
  

  dispatch(setEventLoading(false));
};


// Delete event
export const delEvent = (id) => dispatch => {
  dispatch(setEventLoading(true));

  axios
    .delete(`${process.env.REACT_APP_HOST}/event/del/${id}`)
    .then(res =>
      dispatch({ type: DEL_EVENT, payload: res.data })
    )
    .then(() =>
      dispatch({ type: EDIT_EVENT_MODAL, payload: false })
    )
    .catch(err =>
      dispatch(setMessage('warning', err.response.data.message))
    );
  
  dispatch(setEventLoading(false));
};


// Get events
export const getCategory = () => dispatch => {
  dispatch(setEventLoading(true));

  axios
    .get(`${process.env.REACT_APP_HOST}/event/category/get`)
    .then(res =>
      dispatch({ type: GET_CATEGORY, payload: res.data })
    )
    .catch(err =>
      dispatch(setMessage('warning', err.response.data.message))
    );

  dispatch(setEventLoading(false));
};

// Set loading statee
export const setEventLoading = (status) => {
  return {type: EVENT_LOADING, payload: status };
};
