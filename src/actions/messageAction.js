import uuid from 'uuid';
import { SET_NOTICE, CLEAR_NOTICE, GET_ERRORS, CLEAR_ERRORS } from './types';

export const setMessage = (type = 'warning', message, timeout = 3000) => dispatch => {
  if (typeof message !== 'string') {
    dispatch({
      type: GET_ERRORS,
      payload: message
    })
    setTimeout(() => dispatch({ type: CLEAR_ERRORS }), timeout);
    return;
  }

  const id = uuid.v4();
  const alert = { [type]: message };
  dispatch({
    type: SET_NOTICE,
    payload: alert
  });

  setTimeout(() => dispatch({ type: CLEAR_NOTICE, payload: id }), timeout);
};
