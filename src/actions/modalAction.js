export const modalAction = (type, isOpen) => dispatch => {
  dispatch({
    type,
    payload: isOpen
  });
};
