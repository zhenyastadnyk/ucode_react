import axios from 'axios';
import formData from '../utils/setFormData';
import { setMessage } from './messageAction';
import { isEmpty } from 'ramda';
import { validateAddCalendar, validateEditCalendar } from '../validation';
import {
  GET_CALENDARS,
  CALENDAR_LOADING,
  GET_COLORS,
  GET_CALENDAR,
  DEL_CALENDAR,
  UPDATE_CALENDAR,
  ADD_CALENDAR,
  ADD_CALENDAR_MODAL,
  EDIT_CALENDAR_MODAL
} from './types';

// Get calendars
export const getCalendars = () => dispatch => {
  dispatch(setCalendarLoading(true));

  axios
    .get(`${process.env.REACT_APP_HOST}/calendar/all/get`)
    .then(res =>
      dispatch({ type: GET_CALENDARS, payload: res.data })
    )
    .catch(err => {
      dispatch(setMessage('warning', err.response.data.message));
    }
  );

  dispatch(setCalendarLoading(false));
};

// Get calendar
export const getCalendar = (id) => dispatch => {
    dispatch(setCalendarLoading(true));

    axios
    .get(`${process.env.REACT_APP_HOST}/calendar/${id}`)
    .then(res =>
      dispatch({ type: GET_CALENDAR, payload: res.data })
    )
    .catch(err => {
      dispatch(setMessage('warning', err.response.data.message));
    }
  );

  dispatch(setCalendarLoading(false));
};

// Delete calendar
export const delCalendar = (id) => dispatch => {
  dispatch(setCalendarLoading(true));

  axios
    .delete(`${process.env.REACT_APP_HOST}/calendar/del/${id}`)
    .then(res =>
      dispatch({ type: DEL_CALENDAR, payload: res.data })
    )
    .catch(err => {
      dispatch(setMessage('warning', err.response.data.message));
    }
  );

  dispatch(setCalendarLoading(false));
};

// Update calendar
export const updateCalendar = (id, calendarData) => dispatch => {
  dispatch(setCalendarLoading(true));

  const { errors } = validateEditCalendar(calendarData);
  if (!isEmpty(errors)) {
    dispatch(setMessage('warning', errors));
    dispatch(dispatch(setCalendarLoading(false)));
    return;
  }
  
  axios
    .put(`${process.env.REACT_APP_HOST}/calendar/${id}`, calendarData)
    .then(res =>
      dispatch({ type: UPDATE_CALENDAR, payload: res.data })
    )
    .then(() =>
      dispatch({ type: EDIT_CALENDAR_MODAL, payload: false })
    )
    .catch(err => {
      dispatch(setMessage('warning', err.response.data.message));
    }
  );

  dispatch(setCalendarLoading(false));
};

// Add calendar
export const addCalendar = (calendarData) => dispatch => {
  dispatch(setCalendarLoading(true));

  const { errors } = validateAddCalendar(calendarData);
  if (!isEmpty(errors)) {
    dispatch(setMessage('warning', errors));
    dispatch(dispatch(setCalendarLoading(false)));
    return;
  }

  axios.defaults.headers['X-CALENDAR-TOKEN'] = process.env.REACT_APP_CALENDAR_SECRET;
  
  const body = formData(calendarData);
  axios
    .post(`${process.env.REACT_APP_HOST}/calendar/add`, body)
    .then(res =>
      dispatch({ type: ADD_CALENDAR, payload: res.data })
    )
    .then(res =>
      dispatch({ type: ADD_CALENDAR_MODAL, payload: false })
    )
    .catch(err => {
      dispatch(setMessage('warning', err.response.data.message));
    }
  );

  dispatch(setCalendarLoading(false));
};

// Get colors
export const getColors = () => dispatch => {
  dispatch(setCalendarLoading(true));

  axios
    .get(`${process.env.REACT_APP_HOST}/calendar/color/get`)
    .then(res =>
      dispatch({ type: GET_COLORS, payload: res.data })
    )
    .catch(err => {
      dispatch(setMessage('warning', err.response.data.message));
    }
  );

  dispatch(setCalendarLoading(false));
};


// Set loading state
export const setCalendarLoading = (status) => {
  return { type: CALENDAR_LOADING, payload: status };
};
