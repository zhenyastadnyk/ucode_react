import axios from 'axios';
import setAuthToken from '../utils/setAuthToken';
import formData from '../utils/setFormData';
import { setMessage } from './messageAction';
import {
  validateRegister,
  validateLogin,
  validateEmail,
  validateChangePass
} from '../validation';
import { isEmpty } from 'ramda';
import {
  GET_ERRORS,
  SET_CURRENT_USER,
  AUTH_LOADING
} from './types';

const config = {
  headers: { 'Accept': 'application/json' }
};

// Register
export const registerUser = (userData) => dispatch => {
  dispatch(authLoading(true))

  const { errors } = validateRegister(userData);
  if (!isEmpty(errors)) {
    dispatch(setMessage('warning', errors));
    dispatch(dispatch(authLoading(false)));
    return;
  }
  
  const body = formData(userData);
  axios
    .post(`${process.env.REACT_APP_HOST}/auth/register`, body, config)
    .then(res =>
      dispatch(setMessage('success', 'Please check your email for activation link.'))
    )
    .catch(err =>
      dispatch(setMessage('warning', err.response.data.message))
    )
  
  dispatch(dispatch(authLoading(false)));
};

// Login
export const loginUser = (userData) => dispatch => {
  dispatch(authLoading(true))

  const { errors } = validateLogin(userData);
  if (!isEmpty(errors)) {
    dispatch(setMessage('warning', errors));
    dispatch(dispatch(authLoading(false)));
    return;
  }

  const body = formData(userData);
  axios
    .post(`${process.env.REACT_APP_HOST}/auth/login`, body, config)
    .then(res => {
      const token = res.data.accessToken;
      localStorage.setItem('jwtToken', token);
      setAuthToken(token);
      dispatch(setCurrentUser());
    })
    .catch(err => {
      dispatch(setMessage('warning', err.response.data.message));
    });

    dispatch(authLoading(false));
};

// Resend registration email
export const resendEmail = (email) => dispatch => {
  dispatch(authLoading(true));

  const { errors } = validateEmail(email);
  if (!isEmpty(errors)) {
    dispatch(setMessage('warning', errors));
    dispatch(dispatch(authLoading(false)));
    return;
  }

  axios
    .get(`${process.env.REACT_APP_HOST}/auth/resend?email=${email}`, config)
    .then(res => {
      dispatch(setMessage('success', 'Please check your email again.'))
    })
    .catch(err => {
      dispatch(setMessage('warning', err.response.data.message));
    });

    dispatch(authLoading(false));
};

// Reset pass
export const resetPass = (userData) => dispatch => {
  dispatch(authLoading(true));

  const { errors } = validateEmail(userData.email);
  if (!isEmpty(errors)) {
    dispatch(setMessage('warning', errors));
    dispatch(dispatch(authLoading(false)));
    return;
  }

  const body = formData(userData);
  axios
    .post(`${process.env.REACT_APP_HOST}/auth/resetpassword`, body, config)
    .then(res => {
      dispatch(setMessage('success', 'Please check your email for password reset link.'))
    })
    .catch(err =>
      dispatch(setMessage('warning', err.response.data.message))
    );

  dispatch(authLoading(false));
};

// Activate user
export const confirmUser = userData => dispatch => {
  dispatch(authLoading(true));
  
  const { pathname, search } = userData;
  axios
    .get(`${process.env.REACT_APP_HOST}${pathname}${search}`, config)
    .then(res => {
      dispatch(setMessage('success', 'Your account has been activated.'));
    })
    .catch(err => {
      dispatch(setMessage('warning', err.response.data.message));
    }
  );

  dispatch(authLoading(false));
};

// Change password
export const changePass = userData => dispatch => {
  dispatch(authLoading(true));

  const { errors } = validateChangePass(userData);
  if (!isEmpty(errors)) {
    dispatch(setMessage('warning', errors));
    dispatch(dispatch(authLoading(false)));
    return;
  }

  axios
    .post(`${process.env.REACT_APP_HOST}/auth/changepassword`, userData)
    .then(res => {
      dispatch(setMessage('success', 'Your password has been reset.' ));
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    )

  dispatch(authLoading(false));
};

// Set logged in user
export const setCurrentUser = () => dispatch => {
  dispatch(authLoading(true));
  
  config.headers.Authorization = `Bearer ${localStorage.jwtToken}`;

  axios
    .get(`${process.env.REACT_APP_HOST}/auth/userdata`, config)
    .then(res => {
      dispatch({
        type: SET_CURRENT_USER,
        payload: res.data
      });
    })
    .catch(err => {
      dispatch(setMessage('warning', err.response.data.message));
    }
  );
};

// Log user out
export const logoutUser = () => dispatch => {
  dispatch(authLoading(true));

  config.headers.Authorization = `Bearer ${localStorage.jwtToken}`;
  axios
    .post(`${process.env.REACT_APP_HOST}/auth/logout`, {}, config)
    .then(res => {
      localStorage.removeItem('jwtToken');
      setAuthToken(false);
      dispatch({
        type: SET_CURRENT_USER,
        payload: {}
      });
    })
    .catch(err => {
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data.message
      });
    });

  dispatch(authLoading(false));
};

export const authLoading = (status) => {
  return {
    type: AUTH_LOADING,
    payload: status
  };
};