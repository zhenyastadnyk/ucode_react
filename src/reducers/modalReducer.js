import { ADD_CALENDAR_MODAL, EDIT_CALENDAR_MODAL, ADD_EVENT_MODAL, EDIT_EVENT_MODAL } from '../actions/types';

const initialState = {
  addEventModal: false,
  editEventModal: false,
  addCalendarModal: false,
  editCalendarModal: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case ADD_CALENDAR_MODAL:
      return {
        ...state,
        addCalendarModal: action.payload
      }
    case EDIT_CALENDAR_MODAL:
      return {
        ...state,
        editCalendarModal: action.payload
      }
    case ADD_EVENT_MODAL:
      return {
        ...state,
        addEventModal: action.payload
      }
    case EDIT_EVENT_MODAL:
      return {
        ...state,
        editEventModal: action.payload
      }
    default:
      return state;
  }
}
