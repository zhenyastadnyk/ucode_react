import { combineReducers } from "redux";
import authReducer from "./authReducer";
import errorReducer from "./errorReducer";
import calendarReducer from "./calendarReducer";
import noticeReducer from "./noticeReducer";
import eventReducer from "./eventReducer";
import modalReducer from "./modalReducer";

export default combineReducers({
  auth: authReducer,
  errors: errorReducer,
  calendar: calendarReducer,
  notice: noticeReducer,
  event: eventReducer,
  modal: modalReducer
});
