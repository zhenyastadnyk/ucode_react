import {
  GET_CALENDARS,
  GET_CALENDAR,
  GET_COLORS,
  DEL_CALENDAR,
  UPDATE_CALENDAR,
  ADD_CALENDAR,
  CALENDAR_LOADING
} from '../actions/types';

const initialState = {
  calendars: [],
  calendar: [],
  delCalendar: false,
  updateCalendar: [],
  addCalendar: [],
  loading: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case CALENDAR_LOADING:
      return {
        ...state,
        loading: action.payload
      };
    case GET_CALENDARS:
      return {
        ...state,
        calendars: action.payload.data
      };
    case GET_CALENDAR:
      return {
        ...state,
        calendar: action.payload.data
      };
    case DEL_CALENDAR:
      return {
        ...state,
        delCalendar: action.payload.data
      };
    case UPDATE_CALENDAR:
      return {
        ...state,
        updateCalendar: action.payload.data
      };
    case ADD_CALENDAR:
      return {
        ...state,
        addCalendar: action.payload.data
      };
    case GET_COLORS:
      return {
        ...state,
        colors: action.payload.data
      };
    default:
      return state;
  }
}
