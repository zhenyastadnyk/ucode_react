import { SET_CURRENT_USER, AUTH_LOADING } from '../actions/types';
import { isEmpty } from 'ramda';

const initialState = {
  isAuthenticated: false,
  loading: false,
  user: {}
};

export default function(state = initialState, action) {
  switch (action.type) {
    case AUTH_LOADING:
      return {
      ...state,
      loading: action.payload
      };
    case SET_CURRENT_USER:
      return {
        ...state,
        isAuthenticated: !isEmpty(action.payload),
        loading: false,
        user: action.payload
      };
    default:
      return state;
  }
}
