import {
  GET_EVENTS,
  EVENT_LOADING,
  GET_CATEGORY,
  UPDATE_EVENT,
  DEL_EVENT,
  GET_EVENTS_SEARCH
} from '../actions/types';

const initialState = {
  events: [],
  search_event: [],
  event: [],
  loading: false,
  category: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case EVENT_LOADING:
      return {
        ...state,
        loading: action.payload
      };
    case GET_EVENTS:
      return {
        ...state,
        events: action.payload.data
      };
    case UPDATE_EVENT:
      return {
        ...state,
        event: action.payload.data
      };
    case DEL_EVENT:
      return {
        ...state,
        event: action.payload.data
      };
    case GET_EVENTS_SEARCH:
      return {
        ...state,
        search_event: action.payload.data
      };
    case GET_CATEGORY:
      return {
        ...state,
        category: action.payload.data
      };
    default:
      return state;
  }
}
